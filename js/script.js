//Loading

imagesLoaded('body', {background: 'section'}, function() {
	document.querySelector('#loadingScreen').className = 'loaded';
	setTimeout(function() {
		AOS.init({
			duration: 1000
		});
	}, 1000);
});

// Animate logo

var logoID = document.querySelector('#logoImg');
var logoFiles = ['ship+tents0.png', 'ship+tents1.png', 'ship+tents2.png', 'ship+tents3.png', 'ship+tents4.png', 'ship+tents5.png', 'ship+tents6.png', 'ship+tents7.png', 'ship+tents8.png', 'ship+tents9.png', 'ship+tents10.png', 'ship+tents11.png', 'ship+tents12.png', 'ship+tents13.png', 'ship+tents14.png', 'ship+tents15.png', 'ship+tents16.png', 'ship+tents17.png', 'ship+tents18.png', 'ship+tents19.png'];
var logoStep = 0;
var aniInt = logoFiles.length/0.3;
var animating = setInterval(animateLogo, aniInt);
clearInterval(animating);

logoID.addEventListener('mouseover', startLogo);
logoID.addEventListener('mouseout', stopLogo);

function startLogo() {
	animating = setInterval(animateLogo, aniInt);
}

function animateLogo() {
	logoStep++;
	if (logoStep == logoFiles.length) {
		logoStep = 0;
	}
	logoID.src = 'images/logo_ani/' + logoFiles[logoStep];
}

function stopLogo() {
	clearInterval(animating);
	logoStep = 0;
	logoID.src = 'images/logo_ani/ship+tents0.png';
}

// Scroll Animation

var docNav = document.querySelectorAll('#nav a');
// add title and logo to this
document.querySelector('#logo a').addEventListener('click', setScrollOffset);
document.querySelector('#titleBtn a').addEventListener('click', setScrollOffset);
document.querySelector('#loadNav a').addEventListener('click', setScrollOffset);
document.querySelector('#footer a').addEventListener('click', setScrollOffset);

for (var i=0; i<docNav.length; i++) {
	docNav[i].addEventListener( 'click', setScrollOffset);
}

// Description Scroll

var descNav = document.querySelectorAll('#descNav a');
var descPages = document.querySelectorAll('#desc div');
var descPageNo = 0;

for (var i = 0; i < descNav.length; i++) {
	descNav[i].addEventListener( 'click', checkLR);
	// descNav[i].addEventListener( 'click', descScroll); // redundant
	descNav[i].addEventListener( 'click', setScrollOffset);
}

// Location check

var locBtn = document.querySelectorAll('#magGlass a');
var histLinks = [];

for (var i=0;i<locBtn.length;i++) {
	locBtn[i].addEventListener('click',setScrollOffset);
	histLinks.push(locBtn[i].id);
}

function checkLR() {
	
	var descNavClick = this.className;
	if (descNavClick == 'left') {
		descPageNo--;
		if (descPageNo < 0) {
			descPageNo = descPages.length - 1;
		}
	}
	else {
		descPageNo++;
		if (descPageNo == descPages.length) {
			descPageNo = 0;
		}
	}
	for (var i = 0; i < descPages.length; i++) {
		descPages[i].className = 'invisible';
		if (i == descPageNo) {
			descPages[i].className = 'visible';
		}
	}
}

// Bubble animation
var pulseInt = 600;
var bubbleAnimation = setInterval(bubblePulse, pulseInt);

function bubblePulse() {
	//console.log('running');
	var pulseTrans = (pulseInt-100)/1000 + 's';
	if ($('#loadNav').css('transform') == 'matrix(1, 0, 0, 1, 0, 0)') {
		$('#loadNav').css('transform', 'scale(1.15)');
		$('#loadNav').css('transition', pulseTrans + ' 0.2s');
	}
	else {
		$('#loadNav').css('transform', 'scale(1)');
		$('#loadNav').css('transition', pulseTrans + ' 0s');
	}
}

document.querySelector('#loadNav').addEventListener('mouseover', function() {clearInterval(bubbleAnimation);});
document.querySelector('#loadNav').addEventListener('mouseout', function() {bubbleAnimation = setInterval(bubblePulse, pulseInt);});

//Location bubble animation
var locBubbleAni = setInterval(locBubblePulse, pulseInt);
var locBubbleIndex = '#hist0' + histIndex;

function locBubblePulse() {
	var pulseTrans = (pulseInt-100)/1000 + 's';
	locBubbleIndex = '#hist0' + histIndex;
	//console.log(histIndex);
	if ($(locBubbleIndex).css('transform') == 'matrix(1, 0, 0, 1, 0, 0)') {
		$(locBubbleIndex).css('transform', 'scale(1.25)');
		$(locBubbleIndex).css('transition', pulseTrans + ' 0.2s');
	}
	else {
		$(locBubbleIndex).css('transform', 'scale(1)');
		$(locBubbleIndex).css('transition', pulseTrans + ' 0s');
	}
}

/*
//Scapped as cannot parse style with DOM

function descScroll() {
	event.preventDefault();
	var descPos = document.getElementById('desc').style.left;
	console.log('descPos= ' + descPos);
	// if clicked class name = left
	if (descNavClick === 'left') {
		descScrollL(descPos);
	}
	else {
	// elseif clicked class name = right
		descScrollR(descPos);
	}
}

function descScrollL(descPos) {
	//onclick change css position to -100vw
	var newDescPos = descPos - 100;
	console.log('newDescPos = ' + newDescPos);
	document.getElementById('desc').style.left = newDescPos;
}

function descScrollR(descPos) {
	//onclick change css position to +100vw
	var newDescPos = descPos + 100;
	document.getElementById('desc').style.left = newDescPos;
}
*/

// Scroll Animation

var scrollOffsetV = 0;
var scrollOffsetH = 0;
var histIndex = 0;


function setScrollOffset() {
	event.preventDefault();
	var section = document.querySelector(this.hash);
	//console.log(section.id);
	if (section.id == 'descA' || section.id == 'descB' || section.id == 'descC' || section.id == 'descD') {
		lastDescPage = section;
	}
	if (section.id == 'desc') {
		section = lastDescPage;
	}
	// Check for history click 
	for (var i=0;i<histLinks.length;i++) {
		if (this.id == histLinks[i]) {
			//console.log(histLinks[i]);
			histIndex = i + 1;
			$(locBubbleIndex).css('transform', 'scale(1)');
		}
	}
	scrollOffsetH = section.offsetLeft;
	scrollOffsetV = section.offsetTop;
	requestAnimationFrame( animateScroll );
}

function animateScroll() {
	var scrollPosH = document.documentElement.scrollLeft || document.body.scrollLeft;
	var scrollPosV = document.documentElement.scrollTop || document.body.scrollTop;
	var scrollDistanceH = Math.round(scrollOffsetH - scrollPosH);
	var scrollDistanceV = Math.round(scrollOffsetV - scrollPosV);
	
	// Calculate horizontal scroll
	if (scrollDistanceH > 0) {
		scrollPosH += Math.ceil(scrollDistanceH / 10);
		requestAnimationFrame(animateScroll);
	}
	else if (scrollDistanceH < 0) {
		scrollPosH += Math.floor(scrollDistanceH / 5);
		requestAnimationFrame(animateScroll);
	}
	
	// Calculate Verticle scroll
	if (scrollDistanceV > 0) {
		scrollPosV += Math.ceil(scrollDistanceV / 10);
		requestAnimationFrame(animateScroll);
	}
	else if (scrollDistanceV < 0) {
		scrollPosV += Math.floor(scrollDistanceV / 5);
		requestAnimationFrame(animateScroll);
	}
	
	document.documentElement.scrollLeft = scrollPosH;
	document.body.scrollLeft = scrollPosH;
	document.documentElement.scrollTop = scrollPosV;
	document.body.scrollTop = scrollPosV;
}

// store description page
var lastDescPage = document.querySelector('#descA');

// Set timeout for bubble appearing

var bubbleCounter = 0;
var bubbleDelay = setInterval(bubbleCounting, 1000); // video is 10 seconds
$( '#loadNav' ).css('opacity','0');
$('#logo').css('opacity','0');
$( '#loadNav' ).css('pointer-events','none');
$( '#logo' ).css('pointer-events','none'); // RESTORE THIS OPTION BEFORE SUBMITTING
/////////////////////////////////////////////////////////////////////////////////////////

function bubbleCounting() {
	if (bubbleCounter > 10) {
		clearInterval(bubbleDelay);
		$( '#loadNav' ).css('opacity','100');
		$('#logo').css('opacity','100');
		$('#logo').css('trasition','0.5s');
		$( '#loadNav' ).css('pointer-events','auto');
		$( '#logo' ).css('pointer-events','auto');
	}
	else {
		//console.log(bubbleCounter);
		bubbleCounter++;
	}
}

// To hide random CSS glitch
document.querySelector('#descLink a').addEventListener('mouseover',function() {$('#descLink p').css('opacity', '100');});
document.querySelector('#descLink a').addEventListener('mouseout',function() {$('#descLink p').css('opacity', '0');});
document.querySelector('#foodLink a').addEventListener('mouseover',function() {$('#foodLink p').css('opacity', '100');});
document.querySelector('#foodLink a').addEventListener('mouseout',function() {$('#foodLink p').css('opacity', '0');});
document.querySelector('#locLink a').addEventListener('mouseover',function() {$('#locLink p').css('opacity', '100');});
document.querySelector('#locLink a').addEventListener('mouseout',function() {$('#locLink p').css('opacity', '0');});
document.querySelector('#histLink a').addEventListener('mouseover',function() {$('#histLink p').css('opacity', '100');});
document.querySelector('#histLink a').addEventListener('mouseout',function() {$('#histLink p').css('opacity', '0');});

// History Tooltip
/* Code interpreted from
https://stackoverflow.com/questions/10586962/how-to-create-popup-boxes-next-to-the-links-when-mouse-over-them */

$('#history a').hover(function(e) {
    $($(this).data('tooltip')).css({
        left: '16vw',
		'margin-right': '16vw',
		'padding-left': '0.5vw',
		'padding-right': '0.5vw',
        top: '360vh',
		display: 'block',
		position: 'absolute',
    }).stop().show(100);
}, function() {
    $($(this).data('tooltip')).hide();
});

// title background change
/*
#(document).ready(function() {
	$(window).resize(function() {
		var winH = $(window).height();
		var winW = $(window).width();
		
	})
})*/
/*
$('feedback').
var feedbackRemText = 200;
*/

//function setupForm() {
 $( '#feedbackForm' ).on( 'submit', checkForm );

//}


function checkForm( event ) {
 event.preventDefault();
 console.log('prevented');
if ( $( '#name' ).val() != "" ) {
	if( $('#feedback').val() != ""){
		this.submit();
	}
}
 else {
 $( '#error' ).fadeIn();
}
}

